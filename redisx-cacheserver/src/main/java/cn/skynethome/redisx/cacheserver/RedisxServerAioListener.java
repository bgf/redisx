package cn.skynethome.redisx.cacheserver;

import java.nio.channels.AsynchronousSocketChannel;

import com.talent.aio.common.ChannelContext;
import com.talent.aio.server.AioServer;
import com.talent.aio.server.intf.ServerAioListener;

import cn.skynethome.redisx.common.RedisxPacket;

/**
  * 项目名称:[redisx-cacheserver]
  * 包:[cn.skynethome.redisx.cacheserver]    
  * 文件名称:[RedisxServerAioListener]  
  * 描述:[一句话描述该文件的作用]
  * 创建人:[陆文斌]
  * 创建时间:[2017年1月16日 下午5:00:36]   
  * 修改人:[陆文斌]   
  * 修改时间:[2017年1月16日 下午5:00:36]   
  * 修改备注:[说明本次修改内容]  
  * 版权所有:luwenbin006@163.com
  * 版本:[v1.0]
 */
public class RedisxServerAioListener implements ServerAioListener<Object, RedisxPacket, Object>
{

    @Override
    public boolean onAfterConnected(ChannelContext<Object, RedisxPacket, Object> channelContext)
    {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public void onBeforeSent(ChannelContext<Object, RedisxPacket, Object> channelContext, RedisxPacket packet)
    {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void onAfterDecoded(ChannelContext<Object, RedisxPacket, Object> channelContext, RedisxPacket packet,
            int packetSize)
    {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void onBeforeClose(ChannelContext<Object, RedisxPacket, Object> channelContext, Throwable throwable,
            String remark)
    {
        // TODO Auto-generated method stub
        
    }

    @Override
    public boolean onAfterAccepted(AsynchronousSocketChannel asynchronousSocketChannel,
            AioServer<Object, RedisxPacket, Object> aioServer)
    {
        // TODO Auto-generated method stub
        return true;
    }

    

}
